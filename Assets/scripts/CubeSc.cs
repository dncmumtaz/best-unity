﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSc : MonoBehaviour
{
    public int identity = 1;
    public int activeBelt1 = 0;
    public int activeBelt2 = 0;
    public int activeBelt3 = 0;
    public int activeBelt4 = 0;
    public int activeBelt5 = 0;
    public int activeBelt6 = 0;
    public int activeBelt7 = 0;
    public int activeBelt8 = 0;
    public int activeBelt9 = 0;
    public int activeBelt10 = 0;
}
